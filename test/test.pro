TEMPLATE = app
TARGET = test
DEPENDPATH += . ../src
INCLUDEPATH += . ../src

SOURCES += test.cpp
CONFIG -= qt
CONFIG += debug
unix:LIBS += -L../src -lwarptree
win32:LIBS += -L ..\src\win32 -lwarptree
