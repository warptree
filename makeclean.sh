#! /bin/sh
make distclean
rm Makefile
rm */Makefile*
rm -r */debug
rm -r */release
rm -r latex images/*.eps
rm treeoflife/tol
rm test/test
rm warp/warp
rm src/libwarptree.a
rm designerplugin/libwarptree.so
