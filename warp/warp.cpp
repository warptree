/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include <QtGui/QImageReader>
#include <QtGui/QImageWriter>
#include <QtGui/QImage>
#include <QtCore/QDebug>
#include <math.h>

QImage
convert(const QImage& in) {
    int w = in.width();
    int h = in.height();
    int xc = w/2;
    int yc = h/2;
    QImage out(in.copy());
    for (int i=0; i<w; ++i) {
        for (int j=0; j<h; ++j) {
            float mx = 2*(i-xc)/(float)w;
            float my = 2*(j-yc)/(float)h;
            float md = sqrt(mx*mx+my*my);
            //qDebug() << mx << " " << my << " " << md;
            float f = 1;
            float fx = fabs(mx);
            float fy = fabs(my);
            if (fy > 0 && fy > fx) {
                f = md/fy;
                md = (my>0) ?my :-my;
            } else if (fx > 0) {
                f = md/fx;
                md = (mx>0) ?mx :-mx;
            }
            mx /= f;
            my /= f;
            int si = (int)(xc*mx + xc);
            int sj = (int)(yc*my + yc);
            si = si<w ?si :w-1;
            si = si<0 ?0  :si;
            sj = sj<h ?sj :h-1;
            sj = sj<0 ?0  :sj;
            out.setPixel(i, j, in.pixel(si, sj));
        }
    }
    return out;
}

int
main(int argc, char** argv) {
    if (argc != 3) {
         qDebug() << "Usage: " << argv[0] << " input.png output.png";
         return 1;
    }
    QImageReader reader(argv[1]);
    QImageWriter writer(argv[2]);
    QImage in(reader.read());
    QImage out(convert(in));
    writer.write(out);
    return 0;
}
