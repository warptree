/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "warptreeplugin.h"
#include "warptreeview.h"
#include "warptreeicon.xpm"
#include <QtCore/QtPlugin>

WarpTreePlugin::WarpTreePlugin(QObject *parent) : QObject(parent) {
    initialized = false;
}
void
WarpTreePlugin::initialize(QDesignerFormEditorInterface * /* core */) {
    if (initialized) {
        return;
    }
    initialized = true;
}
bool
WarpTreePlugin::isInitialized() const {
    return initialized;
}
QWidget*
WarpTreePlugin::createWidget(QWidget* parent) {
    return new WarpTreeView(parent);
}
QString
WarpTreePlugin::name() const {
    return "WarpTreeView";
}
QString
WarpTreePlugin::group() const {
    return "Item Views (Model-Based)";
}
QIcon
WarpTreePlugin::icon() const {
    return QIcon(warptree_xpm);
}
QString
WarpTreePlugin::toolTip() const {
    return "This widget is shows a hierarchical data model in a warped hyperbolic plane.";
}
QString
WarpTreePlugin::whatsThis() const {
    return toolTip();
}
bool
WarpTreePlugin::isContainer() const {
    return false;
}
QString
WarpTreePlugin::domXml() const {
    QString dom = "<widget class=\"" + name() + "\" name=\"" + name() + "\">\n";
    dom += " <property name=\"geometry\">\n"
            "  <rect>\n"
            "   <x>0</x>\n"
            "   <y>0</y>\n"
            "   <width>400</width>\n"
            "   <height>400</height>\n"
            "  </rect>\n"
            " </property>\n"
            " <property name=\"toolTip\" >\n"
            "  <string>" + toolTip() + "</string>\n"
            " </property>\n"
            " <property name=\"whatsThis\" >\n"
            "  <string>" + whatsThis() + "</string>\n"
            " </property>\n"
            "</widget>\n";
    return dom;
}
QString
WarpTreePlugin::includeFile() const {
    return "warptreeview.h";
}
Q_EXPORT_PLUGIN2(customwidgetplugin, WarpTreePlugin)
