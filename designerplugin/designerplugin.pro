TEMPLATE = lib

TARGET = warptree
DEPENDPATH += . ../src
INCLUDEPATH += . ../src

CONFIG += designer plugin debug_and_release
CONFIG(debug, debug|release) {
    mac: TARGET = $$join(TARGET,,,_debug)
    win32: TARGET = $$join(TARGET,,d)
}

HEADERS += warptreeplugin.h
SOURCES += warptreeplugin.cpp

unix:LIBS += -L../src -lwarptree
win32 {
    debug:LIBS += -L..\src\debug -lwarptree
    release:LIBS += -L..\src\release -lwarptree
}

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS += target
