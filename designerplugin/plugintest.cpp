/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "warptreeplugin.h"
#include <QtCore/QDebug>
#include <QtGui/QApplication>
#include <QtGui/QWidget>
#include <QtGui/QDirModel>
#include <QtGui/QAbstractItemView>

int main(int argc, char** argv) {
    QApplication app(argc, argv);
    QDirModel model;
    QString path = QDir::currentPath();
    model.index(path);
    WarpTreePlugin plugin;
    qDebug() << plugin.domXml();
    qDebug() << plugin.includeFile();
    QAbstractItemView* view
        = dynamic_cast<QAbstractItemView*>(plugin.createWidget(0));
    if (view) {
        view->setModel(&model);
        view->setRootIndex(model.index(path));
        view->show();
    }
    return app.exec();
}
