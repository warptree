#! /bin/bash
rm -rf html latex images/*.eps
doxygen
# uncomment for generating a pdf manual
#for f in images/*.png; do convert $f ${f/png/eps}; done
#for f in images/*.jpg; do convert $f ${f/jpg/eps}; done
#cd latex && make refman.pdf
