TEMPLATE = app
TARGET = tol
DEPENDPATH += . ../src
INCLUDEPATH += . ../src

HEADERS += warptreeview.h tolmodel.h modelnode.h
SOURCES += tolmodel.cpp tol.cpp
unix:LIBS += -L../src -lwarptree
win32 {
    debug:LIBS += -L..\src\debug -lwarptree
    release:LIBS += -L..\src\release -lwarptree
}
CONFIG += debug_and_release
QT += xml sql
