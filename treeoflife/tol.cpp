/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "tolmodel.h"
#include "warptreeview.h"
#include <QtGui/QTreeView>
#include <QtGui/QSplitter>
#include <QtGui/QApplication>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>

int
main(int argc, char** argv) {
    QApplication app(argc, argv);
    QString filename;
    if (argc > 1) {
        filename = argv[1];
        if (filename == "-h" || filename == "--help") {
            qDebug() << "Usage: " << argv[0] << " treeoflifedata";
        }
    } else {
        // infer the location of the data file from the executable
        QFileInfo file(QFileInfo(argv[0]).dir(), "treeoflife.db");
        if (file.exists()) {
            filename = file.absoluteFilePath();
        } else {
            qDebug() << "Usage: " << argv[0] << " treeoflifedata";
            return 1;
        }
    }
    TolModel model(filename);
    if (model.nodeCount() == 0) {
        QFile file(filename);
        bool ok;
        if (filename == "-") {
            ok = file.open(stdin, QIODevice::ReadOnly);
        } else {
            ok = file.open(QIODevice::ReadOnly);
        }
        if (!ok) {
            qDebug() << "Could not read from " << filename;
                return -1;
            }
        model.index(file);
    }

    QSplitter w;
    QTreeView tree;
    tree.setModel(&model);
    tree.setRootIndex(model.index(0,0));
    for(int i=1; i<10; ++i) {
        tree.setColumnHidden(i, true);
    }

    WarpTreeView warptree;
    WarpNodePainter painter(&warptree);
    warptree.setWarpNodePainter(&painter);

    warptree.setModel(&model);
    warptree.setRootIndex(model.index(0,0));
    warptree.setSelectionModel(tree.selectionModel());

    w.addWidget(&tree);
    w.addWidget(&warptree);
    w.setStretchFactor(0, 0);
    w.setStretchFactor(1, 1);
    w.show();

    w.resize(1000, 500);

    return app.exec();
}
