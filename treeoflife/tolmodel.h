/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef TOLMODEL_H
#define TOLMODEL_H

#include <QtCore/QAbstractItemModel>

class TolNode;
class TolModel : public QAbstractItemModel {
private:
    QList<TolNode> nodes;
    const TolNode* getTolNode(const QModelIndex& mi) const;
public:
    TolModel(const QString& dbfile = QString());
    ~TolModel();
    QModelIndex index(QIODevice& io);
    int columnCount(const QModelIndex& parent) const;
    QVariant data(const QModelIndex& index, int role) const;
    QModelIndex index(int row, int column,
        const QModelIndex& parent = QModelIndex()) const;
    QModelIndex parent ( const QModelIndex & index) const;
    int rowCount ( const QModelIndex & parent) const;
    int nodeCount() const { return nodes.size(); }
};

#endif
