TEMPLATE = app
TARGET = xmltosqlite
QT += sql xml
INCLUDEPATH += . ..

# Input
HEADERS += ../tolmodel.h
SOURCES += xmltosqlite.cpp ../tolmodel.cpp

build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

