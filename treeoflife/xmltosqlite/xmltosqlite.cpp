#include <QtCore>
#include <QtSql>
#include "tolmodel.h"

int
addNode(QSqlQuery& q, QModelIndex& i, int id, int parent) {
    q.bindValue(0, id);
    q.bindValue(1, parent);
    q.bindValue(2, i.data());
    q.bindValue(3, i.data(Qt::ToolTipRole));
    q.exec();
    int p = 0;
    int nextid = id+1;
    QModelIndex child = i.child(p++, 0);
    while (child.isValid()) {
        nextid = addNode(q, child, nextid, id);
        child = i.child(p++, 0);
    }
    return nextid;
}

int
main(int argc, char** argv) {
    QCoreApplication app(argc, argv);
    if (argc != 3) {
        qDebug() << "Usage: " << argv[0] << " treeoflife.xml database.db";
        return 1;
    }
    QString xmlfile = argv[1];
    QString databasefile = argv[2];

    // read the xml
    QFile file(xmlfile);
    bool ok;
    if (xmlfile == "-") {
        ok = file.open(stdin, QIODevice::ReadOnly);
    } else {
        ok = file.open(QIODevice::ReadOnly);
    }
    if (!ok) {
        qDebug() << "Could not read from " << xmlfile;
        return 1;
    }
    TolModel model;
    QModelIndex index = model.index(file);
    if (!index.isValid()) {
        qDebug() << "Could not read xml.";
        return 1;
    }

    // init the sqlite db
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(databasefile);
    if (!db.open()) {
        qDebug() << "Cannot write to " << databasefile;
        return 1;
    }

    // create the table
    db.exec("PRAGMA synchronous = OFF;");
    db.exec("create table phylogeny ("
        "id integer, parent integer, name text, desc text);");
// we do not need this index at the moment
//    db.exec("create index parentidx on phylogeny(parent)");

    QSqlQuery q(db);
    q.prepare("insert into phylogeny (id, parent, name, desc) "
        "values(?,?,?,?);");
    addNode(q, index, 0, -1);

    db.close();

    return 0;
}
