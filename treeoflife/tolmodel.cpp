/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "tolmodel.h"
#include <QtXml/QXmlDefaultHandler>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtCore/QFileInfo>
#include <QtCore/QDebug>

class TolNode {
public:
    QString name;
    QString description;
    QList<int> children;
    int parentid;

    TolNode() :parentid(-1) {}
};

class TolContentHandler : public QXmlDefaultHandler {
private:
    bool characters(const QString& ch) {
        if (depth == 3+2*nodedepth) {
            if (activeName == "NAME") {
                nodes[ids[nodedepth]].name.append(ch);
            } else if (activeName == "DESCRIPTION") {
                nodes[ids[nodedepth]].description.append(ch);
            }
        }
        return true;
    }
    bool startElement(const QString& /*namespaceURI*/, const QString& localName,
            const QString& /*qName*/, const QXmlAttributes& /*atts*/) {
        if (depth == 0) {
             if (localName != "TREE") {
                 error = "Root element is not 'TREE'";
                 return false;
             }
        } else if (depth%2 == 0) {
             // information node
             if (localName == "NODES") {
                 if (ids.size() == ++nodedepth) {
                     ids.append(-1);
                 }
             } else {
                 activeName = localName;
             }
        } else if (localName == "NODE") {
             if (depth == 1) {
                 if (nodes.size()) {
                     error = "File contains more than one root node.";
                     return false;
                 }
                 nodes.append(TolNode());
                 ids.append(0);
             } else if (nodes.size() && depth/2 == nodedepth) {
                 int nid = nodes.size();
                 int pid = ids[nodedepth-1];
                 ids[nodedepth] = nid;
                 nodes.append(TolNode());
                 TolNode& n = nodes.last();
                 n.parentid = pid;
                 nodes[pid].children.append(nid);
             }
        }
        depth++;
        return true;
    }
    bool endElement(const QString&/*namespaceURI*/, const QString& localName,
             const QString&/*qName*/) {
        if (localName == "NODES" && depth/2 == nodedepth) {
            nodedepth--;
        }
        depth--;
        return true;
    }
    QList<TolNode>& nodes;
    QList<int> ids;
    QString activeName;
    QString error;
    int depth;
    int nodedepth;
public:
    TolContentHandler(QList<TolNode>& n) :nodes(n), depth(0), nodedepth(0) {}
    QString errorString() const { return error; }
};

TolModel::TolModel(const QString& dbfile) {
    if (dbfile.isEmpty()) return;
    QFileInfo dbinfo(dbfile);
    if (!dbinfo.exists()) return;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbfile);
    if (!db.open()) {
        return;
    }
    db.exec("PRAGMA synchronous = OFF;");
    QSqlQuery q("select id, parent, name, desc from phylogeny order by id", db);
    TolNode n;
    int id = 0;
    while (q.next()) {
        if (q.value(0) != id) {
            return;
        }
        n.parentid = q.value(1).toInt();
        if (n.parentid >= nodes.size() || (n.parentid < 0 && id != 0)) {
            nodes.clear();
            return;
        }
        n.name = q.value(2).toString();
        n.description = q.value(3).toString();
        if (n.parentid >= 0) {
            nodes[n.parentid].children.append(id);
        }
        nodes.append(n);
        id++;
    }
    db.close();
}
TolModel::~TolModel() {
}
QModelIndex
TolModel::index(QIODevice& io) {
    nodes.clear();

    QXmlSimpleReader reader;
    TolContentHandler handler(nodes);
    reader.setContentHandler(&handler);
    if (!reader.parse(&io)) {
        qDebug() << "Error: " << handler.errorString();
        nodes.clear();
    }
    qDebug() << "found " << nodes.size() << " nodes";
    io.close();
    return index(0, 0);
}
const TolNode*
TolModel::getTolNode(const QModelIndex& mi) const {
    int id = mi.isValid() ?mi.internalId() :0;
    if (id >= nodes.size()) {
        qDebug() << "invalid id: " << id;
    }
    return (id >= 0 && id < nodes.size()) ?&nodes[id] :0;
}
int
TolModel::columnCount(const QModelIndex& parent) const {
    const TolNode* n = getTolNode(parent);
    return n ?1 :0;
}
QVariant
TolModel::data(const QModelIndex& index, int role) const {
    bool descrole = role == Qt::ToolTipRole || role == Qt::StatusTipRole
        || role == Qt::WhatsThisRole;
    if (role != Qt::DisplayRole && !descrole) return QVariant();
    const TolNode* n = getTolNode(index);
    if (n == 0) {
         return QVariant();
    }
    if (descrole) {
        return n->description;
    }
    if (n->name.isEmpty()) {
        return "-";
    }
    return n->name;
}
QModelIndex
TolModel::index(int row, int col, const QModelIndex& parent) const {
    if (!parent.isValid()) {
        return createIndex(0, 0, 0);
    }
    const TolNode* n = getTolNode(parent); 
    if (n == 0 || col != 0 || row < 0 || row >= n->children.size()) {
        return QModelIndex();
    }
    return createIndex(row, col, n->children[row]);
}
QModelIndex
TolModel::parent(const QModelIndex& index) const {
    if (index.column()) return QModelIndex();
    const TolNode* n = getTolNode(index);
    if (n == 0) {
        return QModelIndex();
    }
    // get the parentid
    int pid = n->parentid;
    // if the parentid is 0, the parent is 'root' which has no parent
    // if it is smaller than 0, the index was root, so again, no parent
    if (pid < 0) return QModelIndex();
    int ppid = nodes[pid].parentid;
    int row = (ppid < 0) ?0 :nodes[ppid].children.indexOf(pid);
    return (row == -1) ?QModelIndex() :createIndex(row, 0, pid);
}
int
TolModel::rowCount(const QModelIndex& parent) const {
    if (parent.column()) return 0;
    const TolNode* n = getTolNode(parent);
    return n ?n->children.size() :0;
}
