/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "warptreeview_p.h"
#include <QtCore/QDebug>
#include <QtCore/QTime>
#include <QtGui/QPainter>
#include <QtGui/QMouseEvent>
#include <QtGui/QPainterPath>
//using namespace std;
using namespace Qt;

// function for printing a WarpCoord over QDebug
QDebug&
operator<<(QDebug& o, const WarpCoord& e) {
    o << "(" << e.x() << "," << e.y() << ")";
    return o;
}

/*== WarpNodePainter ==*/

WarpNodePainter::WarpNodePainter(QWidget* w)
    :QObject(w), fontmetrics(w->fontMetrics()), widget(w) {
}
WarpNodePainter::~WarpNodePainter() {}
/*
 * The default implementation does not draw anything in the background.
 */
void
WarpNodePainter::paintBackground(QPainter& /*painter*/, const QSize& /*size*/,
        const WarpTreeNode& /*activeNode*/) {
}
/*
 * The default implementation which uses the palette of the application
 * to draw simple nodes with text and, if available, an icon.
 */
void
WarpNodePainter::paintNode(QPainter& painter, const WarpTreeNode& node,
        bool active, bool loaded, bool hovered) {
    const QWidget* widget = static_cast<const QWidget*>(parent());
    const QPalette& palette = widget->palette();
    QColor c;
    if (hovered) {
        c = palette.color(QPalette::QPalette::AlternateBase);
    } else if (active) {
        c = palette.color(QPalette::QPalette::Highlight);
    } else {
        c = palette.color(QPalette::QPalette::Base);
    }
    c.setAlpha(224);
    painter.fillRect(node.rect, c);
    if (active && !hovered) {
        painter.setPen(palette.color(QPalette::HighlightedText));
    }
    QIcon icon(node.index.data(Qt::DecorationRole).value<QIcon>());
    QString text = node.index.data().toString();
    if (icon.isNull()) {
        painter.drawText(node.rect, Qt::AlignBottom|Qt::AlignCenter, text);
    } else {
        QIcon::Mode mode;
        if (hovered) {
            mode = QIcon::Active;
        } else if (active) {
            mode = QIcon::Selected;
        } else if (loaded) {
            mode = QIcon::Normal;
        } else {
            mode = QIcon::Disabled;
        }
        icon.paint(&painter, node.rect, Qt::AlignLeft, mode);
        painter.drawText(node.rect, Qt::AlignBottom|Qt::AlignRight, text);
    }
    
    painter.setPen(palette.color(QPalette::Text));
}
/*
 * Draw a simple line from parent to node. The line is thicker on the side of
 * the parent to show in which direction the root of the graph is.
 */
void
WarpNodePainter::paintLine(QPainter& painter, const QPoint& parent,
        const QPoint& node, int /*depth*/) {
    QPoint p1(parent.x()+1, parent.y()+1);
    QPoint p2(parent.x()-1, parent.y()-1);
    QPainterPath path(node);
    path.lineTo(p1);
    path.lineTo(p2);
    path.lineTo(node);
    painter.setBrush(Qt::black);
    painter.drawPath(path);
}
/*
 * Calculate the size of the label from the text in the label and the possibly
 * occuring icon.
 */
QSize
WarpNodePainter::getLabelSize(const QModelIndex& index, int /*depth*/) const {
    QIcon icon(index.data(Qt::DecorationRole).value<QIcon>());
    QString text = index.data().toString();
    QSize s = fontmetrics.boundingRect(text).size();
    if (icon.isNull()) {
        s.setWidth(s.width() + fontmetrics.height());
    } else {
        s.setWidth(s.width()+16);
    }
    return s;
}

/*== WarpTree ==*/

void
WarpTree::paintParentLine(QPainter& painter, const ModelNode* node) {
    const ParentModelNode* parent = node->parent;
    if (parent == 0) return;
    paintChildLines(painter, parent, node);
    if (parent->showLine) {
        paintParentLine(painter, parent);
    }
}
void
WarpTree::paintChildLines(QPainter& painter, const ModelNode* node,
        const ModelNode* notnode) {
    const ParentModelNode* pn
        = dynamic_cast<const ParentModelNode*>(node);
    if (pn == 0) return;
    // above a certain amount of children, we do not draw all of them
    int mod = pn->children.size()/50 + 1;
    int count = 0;
    foreach (const ModelNode* n, pn->children) {
        if (n->showLine) {
            if (n != notnode) {
                paintChildLines(painter, n, 0);
            }
            if (mod == 0 || ++count % mod == 0 || n->showText
                    || dynamic_cast<const ParentModelNode*>(n)) {
                nodepainter->paintLine(painter, node->node.rect.center(),
                    n->node.rect.center(), n->node.depth);
                count = 0;
            }
        }
    }
}
void
WarpTree::paintEvent(QPaintEvent* e) {
    QPainter painter(this);

    // check if there is anything to see at all
    if (viewp.activeNode == 0) return;

    painter.setRenderHint(QPainter::Antialiasing, antialias);
    painter.setRenderHint(QPainter::TextAntialiasing, antialias);
    painter.setRenderHint(QPainter::SmoothPixmapTransform, antialias);

    if (viewp.hoverNode && e->rect() == viewp.hoverNode->node.rect) {
        paintNode(painter, viewp.hoverNode);
        return;
    }
    if (viewp.lastHoverNode && e->rect() == viewp.lastHoverNode->node.rect) {
        paintNode(painter, viewp.lastHoverNode);
        return;
    }

    nodepainter->paintBackground(painter, size(), viewp.activeNode->node);

    paintParentLine(painter, viewp.activeNode);
    paintChildLines(painter, viewp.activeNode, 0);
    if (viewp.zoomFactor < 0.3) return;

    foreach(const ModelNode* node, viewp.visibleNodes) {
        if (node->showText) {
            paintNode(painter, node);
        }
    }
}
void
WarpTree::paintNode(QPainter& painter, const ModelNode* node) {
    const ParentModelNode* cn
        = dynamic_cast<const ParentModelNode*>(node);
    bool active = viewp.activeNode == node;
    bool loaded = cn == 0 || cn->complete;
    bool hovered = viewp.hoverNode == node;
    nodepainter->paintNode(painter, node->node, active, loaded, hovered);
}
void
WarpTree::animateTo(const WarpCoord& t) {
    viewp.hoverNode = 0;
    start = viewp.origin;
    end = t;
    animationTimeLine.stop();
    animationTimeLine.start();
}
void
WarpTree::step(qreal value) {
    float x = (1-value)*start.x() + value*end.x();
    float y = (1-value)*start.y() + value*end.y();
    viewp.origin.set(x, y);
    viewp.setOrigin(viewp.origin);
}
void
WarpTreeView::Private::filterChildrenVisible(const QVector<ModelNode*>& nodes) {
    int count = nodes.count();
    // determine which of the labels of the dynamically loaded nodes to show
    for (int i=0; i<count; ++i) {
        ModelNode* n = nodes[i];
        n->showText = n->showLine;
        if (n->showText) {
            for (int j=0; j<i; ++j) {
                ModelNode* m = nodes[j];
                if (m->showText && n->node.rect.intersects(m->node.rect)) {
                    n->showText = false;
                    break;
                }
            }
            if (n->showText && labelfitter.fit(n->node.rect,
                    n->node.rect.center())) {
                visibleNodes.push_back(n);
            }
        }
        ParentModelNode* cn = dynamic_cast<ParentModelNode*>(n);
        if (cn) {
            filterChildrenVisible(cn->children);
        }
    }
}
// function called by the timer to load more items that should be displayed
void
WarpTree::loadMore() {
    viewp.loadMore();
}
void
WarpTreeView::Private::loadMore() {
    QList<ParentModelNode*>::iterator i = todo.begin();
    QTime t;
    t.start();
    float halfwidth = tree.width() / 2.0;
    float halfheight = tree.height() / 2.0;
    // loop during maximally 20 ms to load more nodes
    while(i != todo.end() && t.elapsed() < 20) {
        ParentModelNode* node = *i;
        completeNode(node);
        todo.erase(i);
        foreach (ModelNode* n, node->children) {
            ParentModelNode* cn = dynamic_cast<ParentModelNode*>(n);
            if (cn && cn->translated.visible()) {
                todo.append(cn);
            }
            n->updateScreenCoords(halfwidth, halfheight);
        }
        filterChildrenVisible(node->children);
        i = todo.begin();
        //qApp->processEvents();
    }
    if (todo.size()) {
        tree.loadTimer.start();
    }
    tree.update();
}

/*== WarpTreeView::Private ==*/

/* constant that indicates how deep inside of the unit circle the drag point
   should be. */
#define D_EPS 1.0-1e-7
void
WarpTreeView::Private::startDrag(int x, int y) {
    dragStart = WarpCoord(-(2.0*x/tree.width()-1), -(2.0*y/tree.height()-1));
    dragStart.unstretch();
    if (dragStart.d() > D_EPS) {
        qDebug() << "dragstart.d() > 0.99";
        return;
    }
    dragStartOrigin = origin;
    dragStart.translate(dragStartOrigin);
    if (dragStart.d() > D_EPS) {
        qDebug() << "dragstart.d() > " << D_EPS << " " << dragStart;
        return;
    }
}
void
WarpTreeView::Private::drag(int x, int y) {
    if (dragStart.d() > D_EPS) {
        qDebug() << "dragstart.d() > " << D_EPS;
        return;
    }
    WarpCoord to(-(2.0*x/tree.width() - 1), -(2.0*y/tree.height() - 1));
    to.unstretch();
    if (to.d() > D_EPS) {
        return;
    }

    float de = to.d2();
    float ds = dragStart.d2();
    float dd = 1.0 - de * ds;
    WarpCoord t((to.x() * (1.0 - ds) - dragStart.x() * (1.0 - de)) / dd,
               (to.y() * (1.0 - ds) - dragStart.y() * (1.0 - de)) / dd);

    if (t.valid()) {
        WarpCoord o, p;
        origin = -dragStartOrigin;
        origin.transform(dragStartOrigin, t);
        setOrigin(-origin);
        updateScreenCoords();
    } else {
        qDebug() << "invalid: " << t.x() << " " << t.y();
    }
}
void
WarpTreeView::Private::zoom(float f) {
    zoomFactor *= f;
    setOrigin(origin);
    updateScreenCoords();
}
ModelNode*
WarpTreeView::Private::findNode(const QPoint& pos) {
    foreach(ModelNode* n, visibleNodes) {
        if (n->showText && n->node.rect.contains(pos)) {
            return n;
        }
    }
    return 0;
}
/* code for a future speed improvement that makes the drawing depend less on the
   total amount of loaded nodes
void
WarpTreeView::Private::translateParent(const ModelNode* node) {
    const ParentModelNode* parent = node->parent;
    if (parent == 0) return;
    translateChildren(parent, node);
    if (parent->showLine) {
        //paintParentLine(painter, parent);
    }
}
void
WarpTreeView::Private::translateChildren(const ModelNode* node,
        const ModelNode* notnode) {
}*/
// function for comparing nodes by distance from the center of the square
int
nodeCmp(const ModelNode* a, const ModelNode* b) {
    return a->translated.d() < b->translated.d();
}
// translate all the nodes with respect to the origin
// this updates all the information that is indepdendent of viewport
void
WarpTreeView::Private::setOrigin(const WarpCoord& o) {
    origin = o;

    tree.loadTimer.stop();
    todo.clear();
    visibleNodes.clear();

    ModelNode* centerNode = 0;
    float d = 100, nd;
    foreach(ModelNode* node, nodes) {
        node->translate(o, zoomFactor);
        nd = node->translated.d();
        if (nd < d) {
            centerNode = node;
            d = nd;
        }
        if (node->translated.visible()) {
            visibleNodes.append(node);
            ParentModelNode* cn = dynamic_cast<ParentModelNode*>(node);
            if (cn && !cn->complete) {
                todo.push_back(cn);
            }
        }
    }

    // sort the visible nodes so that the fitting of the labels starts from the
    // labels near the center
    qSort(visibleNodes.begin(), visibleNodes.end(), nodeCmp);
    if (todo.size()) {
        qSort(todo.begin(), todo.end(), nodeCmp);
        tree.loadTimer.start();
    }

    // check if the node in the middle is already the active node
    if (centerNode != activeNode) {
        activeNode = centerNode;
        QModelIndex index = activeNode ?activeNode->node.index :QModelIndex();
        view.setCurrentIndex(index);
        emit view.activated(index);
    }
    updateScreenCoords();
}
// update the screen coordinates and determine which labels to paint
void
WarpTreeView::Private::updateScreenCoords() {
    labelfitter.reset(tree.width(), tree.height());

    float halfwidth = view.width() / 2.0;
    float halfheight = view.height() / 2.0;

    // layout the active node first to make sure it is visible
    // and no label overlaps it
    if (activeNode) {
        activeNode->updateScreenCoords(halfwidth, halfheight);
        labelfitter.fit(activeNode->node.rect, activeNode->node.rect.center());
    }

    foreach (ModelNode* node, nodes) {
        node->updateScreenCoords(halfwidth, halfheight);
    }
    foreach (ModelNode* node, visibleNodes) {
        node->showText = activeNode == node
            || ((node->parent == 0 || node->showLine)
                && labelfitter.fit(node->node.rect, node->node.rect.center()));
    }
    view.update();
}
void
WarpTreeView::Private::setModel(QAbstractItemModel* model) {
    foreach (ModelNode* n, nodes) {
        delete n;
    }
    nodes.clear();
    QModelIndex node = root;
    if (node.model() != model) return;

    ModelNode* n = addNode(node, 0, 0);
    n->layout(0, M_PI, 0.3);
    setOrigin(origin);
}
ModelNode*
WarpTreeView::Private::addNode(const QModelIndex& index,
        ParentModelNode* p, int depth) {
    int count = view.model()->rowCount(index);
    ModelNode* n;
    if (count == 0) {
        n = new ModelNode(depth);
    } else {
        ParentModelNode* cn = new ParentModelNode(depth);
        n = cn;
        cn->count = count;
        cn->weight += log(count);
    }
    n->parent = p;
    n->node.index = index;
    n->node.rect = QRect(QPoint(),tree.nodepainter->getLabelSize(index, depth));
    nodes.insert(index, n);
    return n;
}
void
WarpTreeView::Private::completeNode(ParentModelNode* n) {
    ParentModelNode* cn = static_cast<ParentModelNode*>(n);
    if (cn == 0) return;
    cn->complete = true;
    for (int i=0; i<cn->count; ++i) {
        QModelIndex child = view.model()->index(i, 0, n->node.index);
        ModelNode* ch = addNode(child, cn, n->node.depth+1);
        if (ch) {
            cn->children.append(ch);
            cn->globalWeight += ch->weight;
        }
    }
    cn->layout(cn->angle, cn->width, cn->length);
    cn->translate(origin, zoomFactor);
    foreach(ModelNode* node, cn->children) {
        node->translate(origin, zoomFactor);
    }
}
void
WarpTreeView::Private::mousePressEvent(QMouseEvent* event) {
    movedSinceClick = false;
    startDrag(event->x(), event->y());
}
void
WarpTreeView::Private::mouseMoveEvent(QMouseEvent* event) {
    if (event->buttons() == Qt::LeftButton) {
        movedSinceClick = true;
        hoverNode = 0;
        drag(event->x(), event->y());
    } else {
        if (hoverNode == 0 || !hoverNode->node.rect.contains(event->pos())) {
            QRect r;
            if (hoverNode) {
                r = hoverNode->node.rect;
            }
            lastHoverNode = hoverNode;
            hoverNode = findNode(event->pos());
            if (hoverNode) {
                if (r.isNull()) {
                    r = hoverNode->node.rect;
                } else {
                    r |= hoverNode->node.rect;
                }
            }
            if (!r.isNull()) {
                tree.update(r);
            }
        }
    }
}
void
WarpTreeView::Private::move(int x, int y) {
    startDrag(tree.width()/2+x, tree.height()/2+y);
    drag(tree.width()/2-x, tree.height()/2-y);
}
void
WarpTreeView::Private::animateTo(int x, int y) {
    WarpCoord c(2.0*x/tree.width()-1, 2.0*y/tree.height()-1);
    c.unstretch();
    WarpCoord d(origin);
    d.translate(-c);
    tree.animateTo(d);
}
void
WarpTreeView::Private::mouseReleaseEvent(QMouseEvent* /*event*/) {
    if (!movedSinceClick && hoverNode) {
        // the user clicked on a label
        view.setCurrentIndex(hoverNode->node.index);
        emit view.clicked(hoverNode->node.index);
    }
}

/*== WarpTreeView ==*/

// includes needed for disabling floating point exceptions on windows
#ifdef Q_WS_WIN
#include <float.h>
#endif

WarpTreeView::WarpTreeView(QWidget* parent)
        : QAbstractItemView(parent), p(new Private(*this)) {

#ifdef Q_WS_WIN
// disable floating point exceptions on windows
_control87(_MCW_EM, _MCW_EM);
#endif

    setHorizontalScrollBarPolicy(ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(ScrollBarAlwaysOff);
    p->tree.resize(500, 500);
    setViewport(&p->tree);
    setMouseTracking(true);
}
WarpTreeView::~WarpTreeView() {
    delete p;
}
void
WarpTreeView::resizeEvent(QResizeEvent* /*e*/) {
    // the whole graph can scale up so we recalculate all coordinates
    p->updateScreenCoords();
}
void
WarpTreeView::paintEvent(QPaintEvent* e) {
    p->tree.paintEvent(e);
}
QRect
WarpTreeView::visualRect(const QModelIndex& current) const {
    ModelNode* n = p->nodes.value(current);
    if (n) {
        return n->node.rect;
    }
    return QRect();
}
void
WarpTreeView::scrollTo(const QModelIndex& current,
        QAbstractItemView::ScrollHint) {
    ModelNode* n = p->nodes.value(current);
    if (n) {
        p->tree.animateTo(-n->original);
    }
}
QModelIndex
WarpTreeView::indexAt(const QPoint& pt) const {
    ModelNode* n = p->findNode(pt);
    if (n) {
        return n->node.index;
    }
    return QModelIndex();
}
QModelIndex
WarpTreeView::moveCursor(QAbstractItemView::CursorAction a, KeyboardModifiers) {
    switch (a) {
    case QAbstractItemView::MoveHome:
    case QAbstractItemView::MoveEnd:
        p->tree.animateTo(WarpCoord());
        break;
    case QAbstractItemView::MoveUp:
        p->move(0, 20);
        break;
    case QAbstractItemView::MoveDown:
        p->move(0, -20);
        break;
    case QAbstractItemView::MoveLeft:
        p->move(20, 0);
        break;
    case QAbstractItemView::MoveRight:
        p->move(-20, 0);
        break;
    default:;
    }
    return QModelIndex();
}
int
WarpTreeView::horizontalOffset() const {
    // offset has no meaning
    return 0;
}
int
WarpTreeView::verticalOffset() const {
    // offset has no meaning
    return 0;
}
bool
WarpTreeView::isIndexHidden(const QModelIndex& current) const {
    ModelNode* n = p->nodes.value(current);
    if (n) {
        return n->translated.visible();
    }
    return false;
}
void
WarpTreeView::setSelection(const QRect&,
        QFlags<QItemSelectionModel::SelectionFlag>) {
    // TODO
}
QRegion
WarpTreeView::visualRegionForSelection(const QItemSelection&) const {
    // TODO
    return QRegion();
}
void
WarpTreeView::setModel(QAbstractItemModel* model) {
    QAbstractItemView::setModel(model);
    p->setModel(model);
}
/*
 * Animate the view to display the requested index in the center of the view.
 */
void
WarpTreeView::currentChanged(const QModelIndex& current,
        const QModelIndex& /*previous*/) {
    ModelNode* n = p->nodes.value(current);
    if (n && n != p->activeNode) {
        p->tree.animateTo(-n->original);
    }
}
void
WarpTreeView::mousePressEvent(QMouseEvent* event) {
    p->mousePressEvent(event);
}
void
WarpTreeView::mouseMoveEvent(QMouseEvent* event) {
    p->mouseMoveEvent(event);
}
void
WarpTreeView::mouseReleaseEvent(QMouseEvent* event) {
    p->mouseReleaseEvent(event);
}
void
WarpTreeView::wheelEvent(QWheelEvent* event) {
    int d = event->delta();
    if (d > 0) {
        p->zoom(d/100.0);
    } else if (d < 0) {
        p->zoom(100.0/-d);
    }
}
void
WarpTreeView::setRootIndex(const QModelIndex& index) {
    p->root = index;
    p->setModel(model());
}
void
WarpTreeView::setWarpNodePainter(WarpNodePainter* painter) {
    if (p->tree.nodepainter != painter) {
        p->tree.nodepainter = painter;
        p->tree.update();
    }
}
void
WarpTreeView::setAntialiased(bool a) {
    if (p->tree.antialias != a) {
        p->tree.antialias = a;
        p->tree.update();
    }
}
bool
WarpTreeView::antialiased() const {
    return p->tree.antialias;
} 
