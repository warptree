/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "labelfitter.h"
#include <QtCore/QDebug>

void
LabelFitter::reset(int width, int height) {
    box.setCoords(0, 0, width, height);
    labels.clear();
}
bool
LabelFitter::fit(QRect& r, const QPoint& p) {
    QRect t(r);
    bool mustMove = !box.contains(r, true);
    if (mustMove) {
        if (t.x() < 1) {
            t.moveLeft(1);
        } else if (t.right() >= box.right()) {
            t.moveRight(box.right()-1);
        }
        if (t.y() < 1) {
            t.moveTop(1);
        } else if (t.bottom() >= box.bottom()) {
            t.moveBottom(box.bottom()-1);
        }
        if (!box.contains(t, true)) {
            return false;
        }
        foreach(const Label& a, labels) {
            if (t.intersects(*a.rect)) return false;
        }
    }
    foreach(const Label& a, labels) {
        if (t.intersects(*a.rect)) return false;
    }
    if (mustMove) {
        r = t;
    }
    labels.push_back(Label(r, p));
    return true;
}
