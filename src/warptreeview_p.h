/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "warptreeview.h"
#include "modelnode.h"
#include "labelfitter.h"
#include <QtCore/QTime>
#include <QtCore/QTimer>
#include <QtCore/QTimeLine>

/*
 * Private class that does the actual work of displaying the WarpTree
 */
class WarpTree : public QWidget {
Q_OBJECT
public:
    // data members

    // reference to the public and private classes of WarpTreeView
    WarpTreeView& view;
    WarpTreeView::Private& viewp;
    // pointer to the nodepainter that paints background, nodes and lines
    WarpNodePainter* nodepainter;
    // timer used for loading incomplete nodes in the main thread
    // (it must be done in the main thread because models are not threadsafe
    // in general)
    QTimer loadTimer;
    // timeline for the animation
    QTimeLine animationTimeLine;
    // start- and endpoint of the animation
    WarpCoord start, end;
    // boolean that determines if the widget is drawn antialiased
    bool antialias;

    WarpTree(WarpTreeView& v, WarpTreeView::Private& p)
            :view(v), viewp(p), antialias(true) {
        // construct a default painter that fits with the palette
        nodepainter = new WarpNodePainter(this);

        // initialize the load timer to load more data in intervals of 100 ms
        loadTimer.setSingleShot(true);
        loadTimer.setInterval(100);
        connect(&loadTimer, SIGNAL(timeout()), this, SLOT(loadMore()));

        // initialize the animation timer
        animationTimeLine.setDuration(500);
        connect(&animationTimeLine, SIGNAL(valueChanged(qreal)),
            this, SLOT(step(qreal)));
    }

    // paint the widget
    void paintEvent(QPaintEvent*);
    // paint one node by calling the node painter
    void paintNode(QPainter&, const ModelNode*);
    // paint the line connecting this node to its parent, if one of the two
    // is visible
    void paintParentLine(QPainter& painter, const ModelNode* node);
    // descent into the hierarchy and draw lines that connect to all visible
    // nodes
    void paintChildLines(QPainter& painter, const ModelNode* node,
        const ModelNode* notnode);
    // perform a smooth animation starting from the current position to t
    void animateTo(const WarpCoord& t);
public slots:
    // when not all visible nodes have been completely loaded this slot is
    // called
    void loadMore();
    // perform one step in the animation
    void step(qreal value);
};
class WarpTreeView::Private {
public:
    // the tree widget that does the drawing
    WarpTree tree;
    // all the nodes that have been loaded
    QHash<QModelIndex, ModelNode*> nodes;
    // a subset of all nodes, namely all nodes that have their center visible
    QList<ModelNode*> visibleNodes;
    // the list of nodes that are currently visible and need to be loaded
    QList<ParentModelNode*> todo;
    // the helper class that fits the labels into the view
    LabelFitter labelfitter;
    // a reference to the public part
    WarpTreeView& view;
    // the root of the model
    QPersistentModelIndex root;
    // the point on the unit square that corresponds to the current position
    WarpCoord origin;
    // the start from where the user started a drag action
    WarpCoord dragStart;
    // the origin corresponding to the position from which the drag started
    WarpCoord dragStartOrigin;
    // a pointer to the node that is currently being hovered by the mouse
    ModelNode* hoverNode;
    // a pointer to the previous node that was hovered by the mouse
    ModelNode* lastHoverNode;
    // a pointer to the active node, the node closest to the center of the view
    ModelNode* activeNode;
    // the zoomfactor of the view
    float zoomFactor;
    // a boolean that is set when we are in a drag action
    bool movedSinceClick;

    Private(WarpTreeView& v) :tree(v, *this), view(v), hoverNode(0),
        lastHoverNode(0), activeNode(0), zoomFactor(1), movedSinceClick(false) {
    }
    ~Private() {
        foreach(ModelNode* n, nodes) {
            delete n;
        }
    }
    /* equivalents to the public functions in WarpTreeView that
       contain the actual implementation */
    void setModel(QAbstractItemModel* model);
    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);

    /* functions for moving the view */
    // change the origin of the view and translate all the points in the graph
    void setOrigin(const WarpCoord& o);
    // update the screen coordinates and determine which labels to display
    void updateScreenCoords();
    // two functions that will later help speedup the view
    //void translateParent(const ModelNode* node);
    //void translateChildren(const ModelNode* node, const ModelNode* notnode);
    // start dragging the view at the given screen coordinates
    void startDrag(int x, int y);
    // drag the view to the given screen coordinates
    void drag(int x, int y);
    // change the zoom factor with a factor f
    void zoom(float f);
    // animate the view from the current positions to the position given in
    // screen coordinates
    void animateTo(int x, int y);
    // move the view by the distance given in screen coordinates
    void move(int x, int y);
    // find the node that displays a label at position pos
    ModelNode* findNode(const QPoint& pos);

    /* functions for loading the nodes */
    // load the nodes from the todo list
    void loadMore();
    // load the children of a node and layout the node
    void completeNode(ParentModelNode* n);
    // add one node and determine the size of its label,
    // but do not yet layout or load the children
    ModelNode* addNode(const QModelIndex& index,
        ParentModelNode* parent, int depth);
    // determine which of the labels of the dynamically loaded nodes to show
    void filterChildrenVisible(const QVector<ModelNode*>& nodes);
};
