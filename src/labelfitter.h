/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef LABELFITTER_H
#define LABELFITTER_H

#include <QtCore/QList>
#include <QtCore/QPoint>
#include <QtCore/QRect>
/*
 * Class that fits as many labels as possible on a grid.
 * Each label is attempted to fit as close to the middle as possible.
 */
class LabelFitter {
private:
    struct Label {
        Label(QRect& r, const QPoint& p) :rect(&r), point(p) {}
        QRect* rect;
        QPoint point;
    };
    QRect box;
    QList<Label> labels;
public:
    /**
     * Reinitialize the area on which to fit the labels and clear all old
     * labels.
     **/
    void reset(int width, int height);
    /**
     * Try to fit an additional label on the grid. If the label fits, true is
     * returned and the position of the label is remembered on the grid.
     * If the label cannot be fitted, false is returned.
     * @param r The rectangle bounding the label. The rectangle gives the
     *          size and position of the label. The position is optimized by the
     *          fitting algorithm.
     * @param p The middle point for the label. The label should be places such
     *          that the center of the label is as close as possible to this
     *          point.
     **/
    bool fit(QRect& r, const QPoint& p);
};

#endif
