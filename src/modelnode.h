/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
// parts of code for layout was adapted from HyperTree published under
// MIT License 2001 www.bouthier.net
#ifndef MODELNODE_H
#define MODELNODE_H

#include "warptreeview.h"
#include "warpcoord.h"
#include <QtCore/QThread>
#include <QtGui/QIcon>
#include <QtCore/QModelIndex>
#include <QtCore/QLine>
#include <QtCore/QVector>

class ParentModelNode;
class ModelNode {
public:
    // the public part of the node (QModelIndex, QRect and depth)
    WarpTreeNode node;
    // the coordinate as originally layed out
    WarpCoord original;
    // the original coordinate translated with respect to the views origin
    WarpCoord translated;
    // the parent of the node
    ParentModelNode* parent;
    // the weight of the node. this depends on the number of children
    float weight;
    // whether to show the line connecting this node to its parent
    bool showLine;
    // whether to draw the label
    bool showText;

    ModelNode(int d) :node(d), parent(0), weight(1) {}
    virtual ~ModelNode() {}
    // lay out a node with respect to its parent
    virtual void layout(float angle, float width, float length);
    // update the screen coordinates from the translated coordinates
    void updateScreenCoords(float halfw, float halfh);
    // move the node with respect to the views origin and zoomfactor
    void translate(const WarpCoord& m, float zoomFactor);
};
class ParentModelNode : public ModelNode {
public:
    // the children of this node
    QVector<ModelNode*> children;
    // the global weight of this node
    float globalWeight;
    // the number of children in this node
    int count;
    // the width (angle) of this node
    float width;
    // the length of the connection of this node to its parent
    float length;
    // the angle of this node
    float angle;
    // have the children of this node been loaded?
    bool complete;

    ParentModelNode(int d) :ModelNode(d), globalWeight(0), complete(false) {}
    // overloaded function for laying out this node
    void layout(float angle, float width, float length);
};

#endif
