TEMPLATE = lib
TARGET = warptree
DEPENDPATH += . 
INCLUDEPATH += .

# Input
HEADERS += labelfitter.h modelnode.h warpcoord.h warptreeview.h warptreeview_p.h
SOURCES += labelfitter.cpp \
           modelnode.cpp \
           warpcoord.cpp \
           warptreeview.cpp
CONFIG += debug_and_release staticlib
