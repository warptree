/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef WARPTREEVIEW_H
#define WARPTREEVIEW_H

#include <QtGui/QAbstractItemView>

/**
 * Class that hold information about one node in the WarpTreeView
 **/
class WarpTreeNode {
public:
    /**
     * Construct a new WarpTreeNode
     **/
    WarpTreeNode(int d) :depth(d) {}
    /**
     * The data belonging to this node.
     **/
    QModelIndex index;
    /** 
     * The bounding rectangle for this node.
     **/
    QRect rect;
    /**
     * The depth in the hierarchy of this node. The root node has depth 0.
     **/
    int depth;
};

/**
 * Class that can show a QAbstractItemModel as a warped tree.
 */
class WarpNodePainter : public QObject {
private:
    const QFontMetrics fontmetrics;
    const QWidget* widget;
public:
    /**
     * Constructs a new WarpNodePainter.
     * Set a model with setModel() to use the WarpTreeView.
     * @param parent The parent may not be zero. It is used to determine the
     *               font and palette.
     **/
    WarpNodePainter(QWidget*);
    /**
     * Destroys the WarpNodePainter.
     **/
    virtual ~WarpNodePainter();
    /**
     * Determine the size of a node with given text and icon.
     **/
    virtual QSize getLabelSize(const QModelIndex& index, int depth) const;
    /**
     * Paint the background of the widget.
     **/
    virtual void paintBackground(QPainter& painter, const QSize& size,
        const WarpTreeNode& activeNode);
    /**
     * Paint one node.
     * @param painter
     * @param node
     * @param active
     * @param loaded
     * @param hovered
     **/
    virtual void paintNode(QPainter& painter, const WarpTreeNode& node,
        bool active, bool loaded, bool hovered);
    /**
     * Paint the line connecting two nodes.
     **/
    virtual void paintLine(QPainter& painter, const QPoint& parent,
        const QPoint& node, int depth);
};

/**
 * Class that can show a QAbstractItemModel as a warped tree.
 */
class WarpTreeView : public QAbstractItemView {
public:
    class Private;
private:
    Private* p;
public:
    /**
     * Constructs an empty WarpTreeView.
     * Set a model with setModel() to use the WarpTreeView.
     **/
    WarpTreeView(QWidget* p = 0);
    /**
     * Destroys the view.
     **/
    ~WarpTreeView();
    /**
     * Return the bounding rectangle of the given QModelIndex.
     **/
    QRect visualRect(const QModelIndex&) const;
    /**
     * Move the given index to the center of the view.
     * The ScrollHint has no effect.
     **/
    void scrollTo(const QModelIndex&, QAbstractItemView::ScrollHint);
    /**
     * Returns the model index of the item at the viewport coordinates.
     **/
    QModelIndex indexAt(const QPoint&) const;
    /**
     * Moves the cursor in the view according to the given cursorAction and
     * keyboard modifiers specified by modifiers.
     **/
    QModelIndex moveCursor(QAbstractItemView::CursorAction,
        Qt::KeyboardModifiers);
    /**
     * Returns the horizontal offset of the view.
     **/
    int horizontalOffset() const;
    /**
     * Returns the vertical offset of the view.
     **/
    int verticalOffset() const;
    /**
     * Returns true if the item referred to by the given index is hidden in
     * the view, otherwise returns false.
     */
    bool isIndexHidden(const QModelIndex&) const;
    /**
     * Applies the selection flags to the items in or touched by the rectangle,
     * rect.
     **/
    void setSelection(const QRect&, QFlags<QItemSelectionModel::SelectionFlag>);
    /**
     * Returns the region from the viewport of the items in the given selection.
     **/
    QRegion visualRegionForSelection(const QItemSelection&) const;
    /**
     * This slot is called when a new item becomes the current item.
     * The previous current item is specified by the previous index, and the new
     * item by the current index.
     **/
    void currentChanged(const QModelIndex& current,
        const QModelIndex& previous);
    /**
     * Handle a paint event.
     **/
    void paintEvent(QPaintEvent* e);
    /**
     * Handle a resize event.
     **/
    void resizeEvent(QResizeEvent* e);
    /**
     * Handle a mouse move event. This is used for dragging the view.
     **/
    void mouseMoveEvent(QMouseEvent* event);
    /**
     * Handle a mouse press event.
     **/
    void mousePressEvent(QMouseEvent* event);
    /**
     * Handle a mouse release event.
     **/
    void mouseReleaseEvent(QMouseEvent* event);
    /**
     * Handle a mouse wheel event by zooming the view in or out.
     **/
    void wheelEvent(QWheelEvent* event);
    /**
     * Change the model of the widget.
     **/
    void setModel(QAbstractItemModel* model);
    /**
     * Change the index that is drawn as the root of the widget.
     **/
    void setRootIndex(const QModelIndex& index);
    /**
     * Set a new painter for the lines and nodes.
     **/
    void setWarpNodePainter(WarpNodePainter* painter);
    /**
     * Change whether the tree is drawn antialiassed or not.
     **/
    void setAntialiased(bool a);
    /**
     * Determine whether the tree is drawn antialiassed or not.
     **/
    bool antialiased() const;
};

#endif
