/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

// parts of code for layout was adapted from HyperTree published under
// MIT License 2001 www.bouthier.net
#include "modelnode.h"
#include <QtCore/QDebug>

void
ModelNode::layout(float angle, float /*width*/, float length) {
    if (parent == 0) return;
    WarpCoord zp(parent->original);
    original.set(length * cos(angle), length * sin(angle));
    original.translate(zp);
}
void
ParentModelNode::layout(float angle, float width, float length) {
    ModelNode::layout(angle, width, length);
    this->angle = angle;
    this->width = width;
    this->length = length;
    if (parent) {
        WarpCoord a(cos(angle), sin(angle));
        a.translate(parent->original);
        a.translate(WarpCoord(-original.x(), -original.y()));
        angle = a.arg();
    }
    float c = cos(width);
    float A = 1 + length * length;
    float B = 2 * length;
    width = acos((A * c - B) / (A - B * c));

#define MODELLENGTH 0.5
    int nbrChild = children.size();
    float l1 = (0.95 - MODELLENGTH);
    float l2 = cos((20.0 * M_PI) / (2.0 * nbrChild + 38.0)); 
    length = MODELLENGTH + (l1 * l2);

    float startAngle = angle - width;
    foreach(ModelNode* child, children) {
        float percent = child->weight / globalWeight;
        float childWidth = width * percent;
        float childAngle = startAngle + childWidth;
        child->layout(childAngle, childWidth, length);
        startAngle += 2.0 * childWidth;
    }
}
void
ModelNode::translate(const WarpCoord& o, float zoomFactor) {
    translated = original;
    translated.translate(o);
    if (zoomFactor < 0.99 || zoomFactor > 1.01) {
        translated.zoom(zoomFactor);
    }
    translated.stretch();
    showLine = parent && translated.valid() && parent->translated.valid()
        && (translated.visible() || parent->translated.visible());
/*    if (showLine) {
        lineCenter.set((original.x() + parent->original.x()) / 2,
            (original.y() + parent->original.y()) / 2);
        lineCenter.translate(o);
        showLine = showLine && lineCenter.valid();
    }*/
}
void
ModelNode::updateScreenCoords(float halfw, float halfh) {
    int w = node.rect.width();
    int h = node.rect.height();
    int x = (int)(((translated.x() + 1) * halfw) - w/2);
    int y = (int)(((translated.y() + 1) * halfh) - h/2);
    w += x-1;
    h += y-1;
    node.rect.setCoords(x, y, w, h);
}
