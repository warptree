/* This file is part of WarpTree
 *
 * Copyright (C) 2007 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "warptreeview.h"
#include <QtGui/QDirModel>
#include <QtGui/QTreeView>
#include <QtGui/QSplitter>
#include <QtGui/QApplication>
#include <QtCore/QtDebug>
#include <QtGui/QImageReader>
#include <QtGui/QPainter>

/**
 * Class that implements custom drawing of the WarpTreeView background.
 * If the active file is an image, it is displayed in the background.
 **/
class ImageNodePainter : public WarpNodePainter {
private:
    // cached image for the last successfully displayed path
    QImage image;
    QString path;
public:
    ImageNodePainter(QWidget* w) :WarpNodePainter(w) {}
    void paintBackground(QPainter& painter, const QSize& size,
        const WarpTreeNode& activeNode);
};
void
ImageNodePainter::paintBackground(QPainter& painter, const QSize& /*size*/,
        const WarpTreeNode& activeNode) {
    const QDirModel* model
        = static_cast<const QDirModel*>(activeNode.index.model());
    if (model == 0) return;
    QString p(model->fileInfo(activeNode.index).absoluteFilePath());
    if (p != path) {
        QImageReader reader(p);
        QImage img(reader.read());
        if (img.isNull()) return;
        image = img;
        path = p;
    }
    if (image.isNull()) return;
    painter.drawImage(0, 0, image);
}

int
main(int argc, char** argv) {
    QApplication app(argc, argv);
    QDirModel model;
    QString path = QDir::currentPath();
    if (argc > 1) {
        path = argv[1];
    } else {
        qDebug() << "No path given, using " << path;
    }
    model.index(path);

    QSplitter w;
    QTreeView tree;
    tree.setModel(&model);
    tree.setRootIndex(model.index(path));
    //tree.setAnimated(true);
    for(int i=1; i<10; ++i) {
        tree.setColumnHidden(i, true);
    }

    WarpTreeView warptree;
    ImageNodePainter painter(&warptree);
    warptree.setWarpNodePainter(&painter);
    warptree.setModel(&model);
    warptree.setRootIndex(model.index(path));
    warptree.setSelectionModel(tree.selectionModel());

    w.addWidget(&tree);
    w.addWidget(&warptree);
    w.setStretchFactor(0, 0);
    w.setStretchFactor(1, 1);
    w.show();

    w.resize(1000, 500);

    return app.exec();
}
