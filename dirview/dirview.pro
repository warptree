TEMPLATE = app
TARGET = dirview
DEPENDPATH += . ../src
INCLUDEPATH += . ../src

SOURCES += main.cpp
CONFIG += debug_and_release
unix:LIBS += -L../src -lwarptree
win32 {
    debug:LIBS += -L..\src\debug -lwarptree
    release:LIBS += -L..\src\release -lwarptree
}
